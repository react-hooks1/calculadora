function CalculadoraService(){

    const SOMA = '+';
    const SUBTRACAO = '-';
    const DIVISAO = '/';
    const MULTIPLICACAO = '*';

    function calcular(numero1, numero2, operacao){
        let resultado;

        switch(operacao){
            case SOMA:
                resultado = numero1 + numero2;
                break;
            case SUBTRACAO:
                resultado = numero1 - numero2;
                break;
            case DIVISAO:
                console.log(operacao)
                resultado = numero1 / numero2;
                break;
            case MULTIPLICACAO:
                resultado = numero1 * numero2;
                break;
            default:
                resultado = 0;
        }

        return resultado;
    }

    function concatenarNumero(atual, concat){
        //Caso contenha apenas 0 ou null, reinicia o valor
        if(atual === '0' || atual === null )
            atual = '';

        //Caso contenha apenas . , concatena com 0 antes do ponto
        if(concat === '.' && atual === '' )
            return '0.';

        //Caso . digitado ja contenha um ponto, apenas retornar
        if(concat === '.' && atual.indexOf('.') > -1 )
            return atual;

        return atual + concat;
    }

    return [calcular,concatenarNumero, SOMA, SUBTRACAO, DIVISAO, MULTIPLICACAO];
}

export default CalculadoraService;